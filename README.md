# DDA:
For DDA I made this application.

```php
$x0 = 0;
$y0 = 0;
$x1 = 11;
$y1 = 40;

$dx = ($x1 - $x0);
$dy = ($y1 - $y0);

$step = null;

if (abs($dx)  >= abs($dy)) {
    $step = abs($dx);
} else {
    $step = abs($dy);
}


$dx = $dx /  $step;
$dy = $dy / $step;

$x = $x0;
$y = $y0;

for ($i=1; $i <=$step; $i++) { 
    $x = ($x + $dx);
    $y = ($y + $dy);

    echo "x:{$x},y:{$y}\n";
}
```



# Image Processing:

In the Image Processing I decrease the quality of the image with removing or decreasing the image's pixels.

I made it with javascript. Here is the main code:

```javascript

let c = document.createElement("canvas");
let img1 = new Image();


img1.onload = function () {

    document.getElementById("image1").remove();
    w = img1.width;
    h = img1.height;
    c.width = w;
    c.height = h;
    ctx = c.getContext('2d');
    ctx.drawImage(img1, 0, 0);
    
    let pixelArr = ctx.getImageData(0, 0, w, h).data;

    let sample_size = 4;

    let p;

    for (let y = 0; y < h; y += sample_size) {
        for (let x = 0; x < w; x += sample_size) {
            p = (x + (y * w)) * 4;
            ctx.fillStyle = "rgba(" + pixelArr[p] + "," + pixelArr[p + 1] + "," + pixelArr[p + 2] + "," + pixelArr[p + 3] + ")";
            ctx.fillRect(x, y, sample_size, sample_size);
        }
    }


    // console.log(pixelArr)

    let img2 = new Image();
    img2.src = c.toDataURL("image/jpeg");
    img2.width = 600;
    document.body.appendChild(img2);


};


img1.src = document.getElementById("image1").src;
```


