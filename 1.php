<?php


$x0 = 0;
$y0 = 0;
$x1 = 11;
$y1 = 40;

$dx = ($x1 - $x0);
$dy = ($y1 - $y0);

$step = null;

if (abs($dx)  >= abs($dy)) {
    $step = abs($dx);
} else {
    $step = abs($dy);
}


$dx = $dx /  $step;
$dy = $dy / $step;

$x = $x0;
$y = $y0;

for ($i=1; $i <=$step; $i++) { 
    $x = ($x + $dx);
    $y = ($y + $dy);

    echo "x:{$x},y:{$y}\n";
}
